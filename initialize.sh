#!/bin/bash

# Run all following commands from inside the repo dir
cd memcached_testapp

export DEBIAN_FRONTEND=noninteractive

#if [ -f /etc/modprobe.d/blacklist.conf ]; then
#    sudo rm /etc/modprobe.d/blacklist.conf
#fi

# if memcached isn't installed, grab dependencies and install
if ! command -v memcached &> /dev/null
then
    echo "installing memcached and dependencies"
    sudo apt-get update -y && sudo apt-get -y --allow-downgrades --allow-remove-essential --allow-change-held-packages -o Dpkg::Options::="--force-confnew" upgrade
    sudo apt-get install -y libevent-dev
    sudo apt-get install -y memcached
fi
if ! command -v memcached &> /dev/null
then
    echo "Could not install memcached. Please try to install it manually"
    exit
fi

# clone the mc-crusher repository
if ! command -v git &> /dev/null
then
    echo "Installing git"
    sudo apt-get install -y git
fi

sudo apt-get install gawk

#remove old mc-crusher directory and rebuild
if [ -d mc-crusher ] 
then
    rm -rf mc-crusher
fi
git clone https://github.com/memcached/mc-crusher.git

cd mc-crusher
make
cd ..

if [ -d results ]  
then
    rm -rf results
fi
mkdir results

#run memcached and start benchmarking
memcached -p 11211 -d -u $USER

